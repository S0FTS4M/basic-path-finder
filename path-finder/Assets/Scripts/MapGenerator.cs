using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{

    public GameObject prefab;

    public GameObject Character;

    Vector3 targetPos = Vector3.zero;

    public int curRow = 0, currCol = 0;

    float decisionTime = 1f, speed = 2f;

    //Haritamızın 5 satır 8 sütundan oluştuğunu belirtir
    public int MaxRows = 5, MaxColumns = 8;

    //Harita
    //  0=> boş alan
    //  1=> Duvar
    //  2=> çıkış
    //  3=> ayak izi
    private int[,] map ={
        {0, 1, 1, 1, 1, 1, 1, 1},
        {0, 0, 0, 1, 1, 1, 1, 1},
        {1, 1, 0, 0, 0, 1, 1, 1},
        {1, 1, 1, 1, 0, 0, 0, 1},
        {1, 1, 1, 1, 1, 1, 0, 2}
    };
    private float rotationSpeed = 5;

    // Start is called before the first frame update
    void Start()
    {
        for (int row = 0; row < MaxRows; row++)
        {
            for (int col = 0; col < MaxColumns; col++)
            {
                if (map[row, col] == 1)
                {
                    Instantiate(
                        prefab,
                        new Vector3(row, 0, col),
                        Quaternion.identity
                    );
                }
            }
        }

        Character.transform.position = targetPos;
    }

    // Update is called once per frame
    void Update()
    {
        //üzerinde olduğumuz noktayı işaretliyoruz ki tekrar buraya dönmeyelim(ayak izimizi bırakıyoruz)
        map[curRow, currCol] = 3;

        if (map[curRow, currCol] == 2)
        {
            Debug.Log("Bitti");
            targetPos = Character.transform.position;
            return;
        }

        //eğer ulaşmam gereken noktaya ulaşmışsam bazı işler yapmak istiyorum
        if (Vector3.Distance(Character.transform.position, targetPos) < .1f)
        {
            //Hedefe ulaştım
            if (currCol + 1 < MaxColumns)
            {
                if (map[curRow, currCol + 1] == 0 || map[curRow, currCol + 1] == 2)
                {
                    currCol++;
                    targetPos = new Vector3(curRow, 0, currCol);
                    return;

                }
            }
            if (curRow + 1 < MaxRows)
            {
                if (map[curRow + 1, currCol] == 0 || map[curRow + 1, currCol] == 2)
                {
                    curRow++;
                    targetPos = new Vector3(curRow, 0, currCol);
                    return;

                }
            }
            if (currCol - 1 >= 0)
            {
                if (map[curRow, currCol - 1] == 0 || map[curRow, currCol - 1] == 2)
                {
                    currCol--;
                    targetPos = new Vector3(curRow, 0, currCol);
                    return;

                }
            }
            if (curRow - 1 >= 0)
            {
                if (map[curRow - 1, currCol] == 0 || map[curRow - 1, currCol] == 2)
                {
                    curRow--;
                    targetPos = new Vector3(curRow, 0, currCol);
                    return;

                }
            }


        }
        else
        {

            //KARAKTERIN YÖNÜNÜ AYARLAMAK İÇİN
            Vector3 LookDirection = targetPos - Character.transform.position;

            if (LookDirection != Vector3.zero)
            {
                Quaternion targetRotation = Quaternion.LookRotation(LookDirection);

                // Smoothly rotate towards the target point.
                Character.transform.rotation = Quaternion.Slerp(
                    Character.transform.rotation,
                    targetRotation,
                    rotationSpeed * Time.deltaTime
                );

                Character.transform.localEulerAngles = new Vector3(
                    Character.transform.localEulerAngles.x,
                    Character.transform.localEulerAngles.y,
                    Character.transform.localEulerAngles.z
                );
            }
            /// SON

            //Hareket et
            Character.transform.position += LookDirection.normalized * Time.deltaTime * speed;
        }


    }
}
